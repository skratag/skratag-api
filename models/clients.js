const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientsSchema = new Schema({
    name: String,
    instagram: String,
    phone: String,
    address: String,
    email: String,
    comment: String,
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Client', ClientsSchema);