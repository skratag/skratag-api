const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Client = require('./clients');
const Product = require('./product');

const OrdersSchema = new Schema({
    number: String,
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Client"
    },
    products: [
        {
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product"
            },
            count: Number
        }
    ],
    status: String,
    payment: Number,
    date: Date,
    comment: String,
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Orders', OrdersSchema);