const express = require('express');
const router = express.Router();

router.use('/api/v1/clients', require('./api/v1/clients'));
router.use('/api/v1/products', require('./api/v1/products'));
router.use('/api/v1/orders', require('./api/v1/orders'));

module.exports = router;
