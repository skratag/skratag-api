const express = require('express');
const router = express.Router();
const Product = require('../../../models/product');

router.get('/', function(req, res, next) {
    Product.find({}, (err, products) => {
        if (err) return res.status(500).send(err);

        res.send({
            "success": true,
            "products": products
        });
    })
});

router.get('/:conditions', function(req, res, next) {
    Product.findOne({"_id": req.params.conditions}, (err, products) => {
        if (err) return res.status(500).send(err);

        res.send({
            "success": true,
            "products": products
        });
    })
});

router.post('/', function(req, res, next) {
    const { name, composition, description, price } = req.body;
    const newProduct = new Product({
        name,
        composition,
        description,
        price
    });

    newProduct.save((err) => {
        if (err) return res.status(500).send(err);

        Product.find({}, (err, products) => {
            if (err) return res.status(500).send(err);

            res.send({
                "success": true,
                "products": products
            });
        });
    })
});

router.put('/:id', function(req, res, next) {
    Product.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
        (err) => {
            if (err) return res.status(500).send(err);

            Product.find({}, (err, products) => {
                res.send({
                    "success": true,
                    "products": products
                });
            });
        }
    )
});

router.delete('/:id', function (req, res, next) {
    Product.deleteOne({ _id: req.params.id }, function(err) {
        if (err) return res.status(500).send(err);

        Product.find({}, (err, products) => {
            res.send({
                "success": true,
                "products": products
            });
        });
    })
});

module.exports = router;
