const express = require('express');
const router = express.Router();
const Orders = require('../../../models/order');

router.get('/', function(req, res, next) {
    Orders
        .find({})
        .populate('client')
        .populate('products.product')
        .exec((err, order) => {
            if (err) return res.status(500).send(err);
            res.send({
                "success": true,
                "orders": order
            });
        })
});

router.get('/:conditions', function(req, res, next) {
    Orders
        .findOne({"_id": req.params.conditions})
        .populate('client')
        .populate('products.product')
        .exec((err, order) => {
            if (err) return res.status(500).send(err);
            res.send({
                "success": true,
                "order": order
            });
        })
});

router.post('/', function(req, res, next) {
    const { client, products, date, comment } = req.body;

    const number = generateOrderNumber();
    const status = 'Принят';
    const payment = 0;

    const newOrder = new Orders({
        number,
        client,
        products,
        status,
        payment,
        date,
        comment
    });

    newOrder.save((err) => {
        if (err) return res.status(500).send(err);

        Orders
            .find({})
            .populate('client')
            .populate('products')
            .exec((err, order) => {
                if (err) return res.status(500).send(err);
                res.send({
                    "success": true,
                    "orders": order
                });
            })
    })
});

router.put('/:id', function(req, res, next) {
    Orders.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
        (err) => {
            if (err) return res.status(500).send(err);

            Orders.find({}, (err, orders) => {
                res.send({
                    "success": true,
                    "orders": orders
                });
            });
        }
    )
});

router.delete('/:id', function (req, res, next) {
    Orders.deleteOne({ _id: req.params.id }, function(err) {
        if (err) return res.status(500).send(err);

        res.send({
            "success": true
        });
    })
});

function generateOrderNumber() {
    const letters = 'QWERTYUIOPASDFGHJKLZXCVBNM';
    const numbers = '1234567890';

    let id = '';

    for (let i = 0; i < 2; i++) {
        id += letters[Math.floor(Math.random() * letters.length)]
    }

    for (let i = 0; i < 4; i++) {
        id += numbers[Math.floor(Math.random() * numbers.length)]
    }

    return id;
}

module.exports = router;