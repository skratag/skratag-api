const express = require('express');
const router = express.Router();
const Client = require('../../../models/clients');

router.get('/', function(req, res, next) {
    Client.find({}, (err, clients) => {
        res.send({
            "success": true,
            "clients": clients
        });
    })
});

router.get('/:conditions', function(req, res, next) {
    Client.findOne({"_id": req.params.conditions}, (err, clients) => {
        if (err) return res.status(500).send(err);

        res.send({
            "success": true,
            "clients": clients
        });
    })
});

router.post('/', function(req, res, next) {
    const { name, phone, address, instagram, email, comment } = req.body;
    const newClient = new Client({
        name,
        phone,
        instagram,
        address,
        email,
        comment
    });

    newClient.save((err) => {
        if (err) return res.status(500).send(err);

        Client.find({}, (err, clients) => {
            if (err) return res.status(500).send(err);

            res.send({
                "success": true,
                "clients": clients
            });
        });
    })
});

router.put('/:id', function(req, res, next) {
    Client.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
        (err) => {
            if (err) return res.status(500).send(err);

            Client.find({}, (err, clients) => {
                res.send({
                    "success": true,
                    "clients": clients
                });
            });
        }
    )
});

router.delete('/:id', function (req, res, next) {
    Client.deleteOne({ _id: req.params.id }, function(err) {
        if (err) return res.status(500).send(err);

        Client.find({}, (err, clients) => {
            res.send({
                "success": true,
                "clients": clients
            });
        });
    })
});

module.exports = router;
